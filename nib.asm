	IDEAL
	MODEL SMALL,C
	P386
	STACK 512

	DATASEG

EXTRN	level_seg: WORD
PUBLIC  p1_left
PUBLIC  p1_right
PUBLIC  p2_left
PUBLIC  p2_right

old_mode 	DW ?			; puvodni graficky mod
VIDEO		EQU 0B800h

logo		DB 'ҿ     � � ���Ŀ   ���Ŀ   �       �����Ŀ   ����Ŀ'
		DB '���    � � �   �   �   �   �       �         �     '
		DB '� ��   � � �����Ŀ �����Ŀ �       ���       �     '
		DB '�  ��  � � �     � �     � �       �         ����Ŀ'
		DB '�   �� � � ������� ������� ������� �������        �'
		DB '�    ���      (c) 1999 Martin Kvapil       �      �'
		DB '�     ��                                   ��������'

item1		DB '     One player     '
item2		DB '    Two  players    '
item3		DB '   Keyboard setup   '
item4		DB '     Quit  game     '

menu_left	EQU 30			; x-ova souradnice menu
menu_top	EQU 26			; y-ova souradnice menu
menu_items	EQU 4			; pocet polozek v menu
item_sel	DW 0			; zvyraznema polozka menu
item_len	DW 20			; velikost polozky v menu
no_attr		DW 09h			; atribut normalni polozky menu
hi_attr		DW 71h			; atribut zvyraznene polozky menu

p1_left_msg	DB 'Press player 1 turn left key '
p1_right_msg	DB 'Press player 1 turn right key'
p2_left_msg	DB 'Press player 2 turn left key '
p2_right_msg	DB 'Press player 2 turn right key'

p1_left		DB 4Bh			; <-
p1_right	DB 4Dh			; ->
p2_left		DB 2Ch			; z
p2_right	DB 2Dh			; x

	ENDS

	CODESEG

EXTRN  play: PROC
PUBLIC	clear, frame

PROC    main
	mov	ax, _DATA
	mov	ds, ax
	mov	bx, sp			; realokuj pamet pro program
	shr	bx, 4
	inc	bx
	mov	ax, ss
	mov	dx, es
	sub	ax, dx
	add	bx, ax
	mov	ah, 4Ah
	int	21h

	mov	bx, 256			; alokuj 4K pameti pro levely
	mov	ah, 48h
	int	21h
	jc	@@1
	mov	[level_seg], ax

	cld
        call    get_mode                ; zjisti stary mod
        mov     [old_mode], ax
        mov     ax, 0103h
	call	set_mode                ; nastav 80x50 mod

        call	main_menu

        mov ax, [old_mode]
	call	set_mode		; nastav puvodni mod

	mov	ax, [level_seg]		; uvolni alokovanou pamet
	mov	es, ax
	mov	ah, 49h
	int	21h

@@1:	mov	ah, 4Ch
	int	21h			; ukonci program
        ret

ENDP

; =============================

PROC    get_mode
        mov     ah, 0fh
        int     10h                     ; zjisti videomod
        push    ax
        mov     ax, 1130h
        xor     bh, bh
        xor     dl, dl
        int     10h                     ; informace o fontu (dl = # radek)
        pop     ax
        cmp     dl, 25
        sbb     ah, ah
        inc     ah
        ret
ENDP

PROC    set_mode
	xor	dx, dx
	mov	es, dx
	and	byte ptr es:[0410h], 0CFh
	or	byte ptr es:[0410h], 020h	; barevny monitor 80 sloupcu
	and	byte ptr es:[0487h], 0FEh	; zakaz emulace kurzoru
        push    ax
	xor	ah, ah
	int	10h			; textovy rezim
        pop     ax
        or      ah, ah
        je      @@1                     ; NEnastavuj font 8x8
        mov     ax, 1112h
        xor     bl, bl
        int     10h                     ; zaved font 8x8
        or      byte ptr es:[0487h], 1  ; povol emulaci kurzoru
	mov	ah, 1
	mov	cx, 0600h
	int	10h			; nastav kurzor
	mov	ah, 12h
	mov	bl, 20h
	int	10h			; nastav stdandardni PRINT-SCREEN
@@1:    ret
ENDP

PROC	write
ARG	src: WORD, x: BYTE, y: BYTE, attr: BYTE, len: WORD
	cld
	mov	si, [src]
	mov	al, [y]
	mov	dl, 80
	mul	dl
	add	al, [x]
	adc	ah, 0
	shl	ax, 1
	mov	di, ax
	mov	ax, VIDEO
	mov	es, ax
	mov	al, [attr]
	mov	cx, [len]
@@1:
	movsb				; znak
	stosb				; atributy
	dec	cx
	jnz	@@1
	ret
ENDP

PROC	st_attr
ARG	attr: BYTE, x: BYTE, y: BYTE, len: WORD
	cld
	mov	al, [y]
	mov	dl, 80
	mul	dl
	add	al, [x]
	adc	ah, 0
	shl	ax, 1
	mov	di, ax
	inc	di
	mov	ax, VIDEO
	mov	es, ax
	mov	al, [attr]
	mov	cx, [len]
@@1:	stosb
	inc	di
	dec	cx
	jnz	@@1
	ret
ENDP

PROC	clear
	cld
	mov	ax, VIDEO
	mov	es, ax
	xor	di, di
	xor	eax, eax
	mov	cx, 80*25
	rep	stosd
	ret
ENDP

PROC	frame
	cld
        mov	ax, VIDEO
        mov	es, ax
        mov	di, 160
	mov	ax, 03C9h
	stosw
        mov     ax, 03CDh
        mov     cx, 78
        rep stosw
	mov	ax, 03BBh
	stosw

	mov	cx, 47
	mov	ax, 03BAh
@@1:	stosw
	add	di, 2*78
	stosw
	dec	cx
	jnz	@@1

	mov	ax, 03C8h
	stosw
        mov     ax, 03CDh
        mov     cx, 78
        rep stosw
	mov	ax, 03BCh
	stosw
	ret
ENDP

PROC	draw_logo
	mov	cx, 7			; 7 radku
	lea	bx, [logo]
	mov	dx, 10			; radek
@@1:	push	cx
	push	51			; delka
	push	0Eh			; zluta barva
	push	dx			; y
	push	14			; x
	push	bx			; adresa
	call	write
	pop	bx
	add	sp, 2
	pop	dx
	add	sp, 4
	pop	cx
	inc	dx			; zvys radek
	add	bx, 51			; posun zacatek
	dec	cx
	jnz	@@1
	ret
ENDP

MACRO	WRITE_TEXT addr, x, y, attr, len
	push	&len
	push	&attr
	push	&y
	push	&x
	push	&addr
	call	write
	add	sp, 10
ENDM

MACRO	SET_ATTR attr, x, y, size
	push	&size
	push	&y
	push	&x
	push	&attr
	call	st_attr
	add	sp, 8
ENDM

PROC    main_menu
	mov	[item_sel], 0
@@st:	call	clear
	call	frame
	call	draw_logo

	lea	dx, [item1]
	mov	ax, menu_top
	WRITE_TEXT dx, menu_left, ax, [no_attr], [item_len]
	lea	dx, [item2]
	mov	ax, menu_top
	inc	ax
	WRITE_TEXT dx, menu_left, ax, [no_attr], [item_len]
	lea	dx, [item3]
	mov	ax, menu_top
	add	ax, 2
	WRITE_TEXT dx, menu_left, ax, [no_attr], [item_len]
	lea	dx, [item4]
	mov	ax, menu_top
	add	ax, 3
	WRITE_TEXT dx, menu_left, ax, [no_attr], [item_len]

	mov	ax, menu_top
	add	ax, [item_sel]
	SET_ATTR [hi_attr], menu_left, ax, [item_len]

@@key:	mov	ah, 0
	int	16h

	cmp	ah, 1Ch			; ENTER
	jne	@up

; ---------
	cmp	[item_sel], 0		; One player
	jne	@two
	push	1
	call	play
	add	sp, 2
	jmp	@@st

@two:	cmp	[item_sel], 1		; Two players
	jne	@setup
	push	2
	call	play
	add	sp, 2
	jmp	@@st

@setup:	cmp	[item_sel], 2		; Keyboard setup
	jne	@quit
	call	setup
	jmp	@@st

@quit:	cmp	[item_sel], 3		; Quit
	jne	@@key
	ret

; ---------
@up:	cmp	ah, 48h			; sipka nahoru
	jne	@down
	mov	ax, menu_top
	add	ax, [item_sel]
	SET_ATTR [no_attr], menu_left, ax, [item_len]
	mov	ax, [item_sel]
	dec	ax
	jns	@@a1
	add	ax, menu_items
@@a1:	mov	[item_sel], ax
	add	ax, menu_top
	SET_ATTR [hi_attr], menu_left, ax, [item_len]
	jmp	@@key

; ---------
@down:	cmp	ah, 50h			; sipka dolu
	jne	@@key
	mov	ax, menu_top
	add	ax, [item_sel]
	SET_ATTR [no_attr], menu_left, ax, [item_len]
	mov	ax, [item_sel]
	inc	ax
	cmp	ax, menu_items
	jb	@@b1
	sub	ax, menu_items
@@b1:	mov	[item_sel], ax
	add	ax, menu_top
	SET_ATTR [hi_attr], menu_left, ax, [item_len]
	jmp	@@key

        ret
ENDP

PROC	setup
@@1:	lea	dx, [p1_left_msg]
	WRITE_TEXT dx, 26, 35, 07h, 29
	mov	ah, 0
	int	16h
	cmp	ah, 1			; escape
	je	@@1
	mov	[p1_left], ah

@@2:	lea	dx, [p1_right_msg]
	WRITE_TEXT dx, 26, 35, 07h, 29
	mov	ah, 0
	int	16h
	cmp	ah, 1			; escape
	je	@@2
	cmp	ah, [p1_left]
	je	@@2
	mov	[p1_right], ah

@@3:	lea	dx, [p2_left_msg]
	WRITE_TEXT dx, 26, 35, 07h, 29
	mov	ah, 0
	int	16h
	cmp	ah, 1			; escape
	je	@@3
	cmp	ah, [p1_left]
	je	@@3
	cmp	ah, [p1_right]
	je	@@3
	mov	[p2_left], ah

@@4:	lea	dx, [p2_right_msg]
	WRITE_TEXT dx, 26, 35, 07h, 29
	mov	ah, 0
	int	16h
	cmp	ah, 1			; escape
	je	@@4
	cmp	ah, [p1_left]
	je	@@4
	cmp	ah, [p1_right]
	je	@@4
	cmp	ah, [p2_left]
	je	@@4
	mov	[p2_right], ah
	ret
ENDP

	ENDS

	END
