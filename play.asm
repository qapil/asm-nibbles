        IDEAL
        MODEL SMALL,C
        P386

        DATASEG

EXTRN   p1_left: BYTE, p1_right: BYTE
EXTRN	p2_left: BYTE, p2_right: BYTE
PUBLIC	level_seg

VIDEO		EQU 0B800h
P1_COLOR	EQU 5720h
P2_COLOR	EQU 2720h
BLANK		EQU 0720h
P1_POS		EQU 2*(24*80+49)
P2_POS		EQU 2*(26*80+30)
START_LEN	EQU 5
TIMER_INIT	EQU 3			; pocatecni delka cekani (rychlost)

ofs_08		DW ?
seg_08		DW ?			; puvodni obsluha casovace
busy		DB 0			; semafor do casovace
timer		DW 0
timer_val	DW 3			; prodleva bude timer_val*55 ms

STRUC	nibble
  score		DW 0			; dosazene skore
  lives		DB 0			; pocet zivotu
  length	DB 0			; aktualni delka
  append	DB 0			; o kolik se ma prodlouzit
  head		DW 0			; pozice hlavy
  body		DW 256 DUP (0)		; pozice tela housenky
  d_buf		DB 16 DUP (0)		; buffer se smery pohybu
  d_size	DB 0			; pocet prvku v bufferu
  color		DW 0			; barva a tributy nibble
  crash		DB 0			; v pripade kolize != 0
ENDS

STRUC	target
  pos		DW 0			; pozice cile
  num		DB 0			; hodnota
ENDS

lives_msg	DB 'l',7,'i',7,'v',7,'e',7,'s',7,':',7,' ',7
score_msg	DB 's',7,'c',7,'o',7,'r',7,'e',7,':',7
wait_msg	DB 'P',14,'r',14,'e',14,'s',14,'s',14,' ',14,'E',14,'s',14,'c',14,'a',14,'p',14,'e',14,' ',14,'t',14,'o',14,' ',14,'c',14,'o',14,'n',14,'t',14,'i',14,'n',14,'u',14,'e',14
p1		nibble ?		; 1. hrac
p2		nibble ?		; 2. hrac
trg		target ?		; cislo, ktere se ma sbirat
collect		DB 0
crash		DB 0
level		DB 0			; cislo levelu
level_path	DB 'levels\level00.nib',0
level_seg	DW 0

        ENDS

        CODESEG

EXTRN	clear: PROC, frame: PROC
PUBLIC  play


PROC	play
ARG	plr_cnt: BYTE
	mov	[timer_val], TIMER_INIT

	xor	ax, ax
	mov	es, ax
	mov	ax, es:[046Ch]
	mov	[trg.pos], ax
	mov	[level], 0
	mov	[p1.score], 0
	mov	[p1.lives], 3
	mov	[p1.color], P1_COLOR

	cmp	[plr_cnt], 2
	je	@@0
	mov	[p2.lives], 0
	jmp	@@1

@@0:	mov	[p2.score], 0
	mov	[p2.lives], 3
	mov	[p2.color], P2_COLOR
	
@@1:	inc	[level]

	push	[level_seg]
	call	load_level
	add	sp, 2
	cmp	ax, 0
	je	@@2
	cmp	ax, 2
	jne	@@3			; chyba pri cteni levelu

	mov	[level], 0		; zrychli a znovu 1. level
	dec	[timer_val]
	jns	@@1
	mov	[timer_val], 1		; nejkratsi mozny interval
	jmp	@@1

@@2:	call    new_timer		; nastav casovac
	call	play_loop		; vlastni hlavni
	push	ax
	call    old_timer		; puvodni casovac
	pop	ax

	cmp	ax, 0			; Escape nebo nikdo nezije
	je	@@1
@@3:	ret
ENDP

PROC	load_level
ARG	s: WORD
	push	ds
	push	ds
	pop	es
	lea	dx, [level_path]	; zmen cislo v seste k levelu
	mov	di, dx
	add	di, 13			; ukazuje na cislo
	xor	ax, ax
	mov	al, [level]
	mov	bl, 10
	std
@@1:	div	bl
	push	ax
	mov	al, ah
	add	al, '0'
	stosb
	pop	ax
	xor	ah, ah
	test	al, al
	jnz	@@1

	mov	ax, 3D00h		; otevri soubor
	int	21h
	jc	@@er
	mov	bx, ax

	mov	ax, [s]			; precti soubor
	mov	ds, ax
	xor	dx, dx
	mov	cx, 47*80
	mov	ah, 3Fh
	int	21h
	jc	@@2
	xor	ax, ax
@@2:	push	ax			; uchovej chybovy kod

	mov	ah, 3Eh			; zavri soubor
	int	21h
	pop	ax

@@er:	pop	ds
	ret
ENDP

PROC	show_level
ARG	s: WORD
	push	ds
	mov	ds, [s]
	mov	ax, VIDEO
	mov	es, ax
	mov	di, 2*2*80
	xor	si, si
	mov	dx, 47
	mov	al, 07h

	cld
@@1:	inc	di			; levy okraj
	inc	di
	mov	cx, 78
@@2:	movsb				; znak
	stosb				; atributy
	dec	cx
	jnz	@@2
	inc	di			; pravy okraj
	inc	di
	inc	si			; preskoc 0Dh, 0Ah (novy radek)
	inc	si
	dec	dx
	jnz	@@1	
	
	pop	ds
	ret
ENDP

PROC	new_target
	inc	[trg.num]		; zvys cislo
	cmp	[trg.num], 10
	jb	@@0
	stc
	ret

@@0:	mov	ax, VIDEO		; zobraz
	mov	es, ax
	mov	cx, [trg.pos]		; generuj pozici

@@1:	mov	ax, cx
	inc	ax
	mov	dx, 0065h
	mul	dx
	dec	ax
	mov	cx, ax
@@2:	cmp	ax, 322			; horni okraj
	jb	@@1
	cmp	ax, 7836		; dolni okraj
	ja	@@1
	mov	bx, 160
	div	bl
	cmp	ah, 2			; levy okraj
	jb	@@1
	cmp	ah, 156			; pravy okraj
	ja	@@1
	and	cx, 0FFFEh
	mov	di, cx
	cmp	es:[di], BLANK
	jne	@@1

	mov	[trg.pos], cx
	mov	al, [trg.num]
	add	al, '0'
	mov	ah, 0Eh
	stosw
	clc
	ret
ENDP

MACRO	SHOW_NUM attr
LOCAL	@@1
	std
	mov	bx, 10
@@1:	xor	dx, dx
	div	bx
	push	ax
	mov	al, dl
	add	al, '0'
	mov	ah, &attr
	stosw
	pop	ax
	test	ax, ax
	jnz	@@1
	cld
ENDM

PROC	show_status
	mov	ax, VIDEO
	mov	es, ax
	cld
	mov	di, 2
	lea	dx, [lives_msg]
	mov	si, dx
	mov	cx, 7
	rep movsw
	xor	ax, ax
	mov	al, [p2.lives]
	SHOW_NUM 7
	mov	di, 22
	lea	dx, [score_msg]
	mov	si, dx
	mov	cx, 6
	rep movsw
	add	di, 8
	mov	ax, [p2.score]
	SHOW_NUM 7

	mov	di, 116
	lea	dx, [lives_msg]
	mov	si, dx
	mov	cx, 7
	rep movsw
	xor	ax, ax
	mov	al, [p1.lives]
	SHOW_NUM 7
	mov	di, 136
	lea	dx, [score_msg]
	mov	si, dx
	mov	cx, 6
	rep movsw
	add	di, 8
	mov	ax, [p1.score]
	SHOW_NUM 7
	ret
ENDP

PROC	get_keys
@@key:	mov	ah, 1
	int	16h
	jz	@@end			; nic neni stisknuto
	mov	ah, 0
	int	16h

	cmp	ah, 1			; escape
	jne	@@1
	mov	ax, 1
	ret

@@1:	push	ds
	pop	es
	cld
	xor	bx, bx

	mov	bl, [p1.d_size]
	cmp	bl, 10h
	jae	@@key			; plny buffer
	lea	si, [p1.d_buf]
	add	si, bx
	mov	di, si
	cmp	bl, 0
	je	@@2
	dec	si
@@2:	cmp	ah, [p1_left]
	jne	@@3
	lodsb				; posledni smer --> al
	inc	al			; otoc
	jmp	@@4
@@3:	cmp	ah, [p1_right]
	jne	@@5
	lodsb				; posledni smer --> al
	dec	al			; otoc
@@4:	and	al, 3
	stosb				; zapis novy smer
	inc	[p1.d_size]
	jmp	@@key
	

@@5:	mov	bl, [p2.d_size]
	cmp	bl, 10h
	jae	@@key			; plny buffer
	lea	si, [p2.d_buf]
	add	si, bx
	mov	di, si
	cmp	bl, 0
	je	@@6
	dec	si
@@6:	cmp	ah, [p2_left]
	jne	@@7
	lodsb				; posledni smer --> al
	inc	al			; otoc
	jmp	@@8
@@7:	cmp	ah, [p2_right]
	jne	@@key
	lodsb				; posledni smer --> al
	dec	al			; otoc
@@8:	and	al, 3
	stosb				; zapis novy smer
	inc	[p2.d_size]
	jmp	@@key

@@end:	xor	ax, ax
	ret				; nebyl stisknuty Escape
ENDP

MACRO	NEW_HEAD x
LOCAL	@@1, @@2, @@3, @@4, @@5, @@6, @@7
	mov	ax, [&x.head]		; stary zacatek nibble
	lea	si, [&x.d_buf]
	mov	bl, ds:[si]		; smer
	push	ax
	push	bx
	xor	ax, ax
	cmp	[&x.d_size], 0		; buffer je prazdny, takze priste se
	jbe	@@1			; bude opakovat stejny smer
	dec	[&x.d_size]
	mov	al, [&x.d_size]
	push	ax			; delka
	inc	si
	push	si			; zdroj
	dec	si
	push	si			; cil
	call	memmove			; posun buffer smeru
	add	sp, 6
@@1:	pop	bx
	pop	ax

	cmp	bl, 0			; doprava
	jne	@@2
	add	ax, 2
	jmp	@@5

@@2:	cmp	bl, 1			; nahoru
	jne	@@3
	sub	ax, 160
	jmp	@@5

@@3:	cmp	bl, 2			; doleva
	jne	@@4
	sub	ax, 2
	jmp	@@5

@@4:	add	ax, 160			; dolu

; kontroly okraje obrazovky

@@5:	push	ax
	cmp	ax, 320			; horni okraj
	jna	@@6
	cmp	ax, 7840		; dolni okraj
	jnb	@@6
	mov	bx, 160
	div	bl
	cmp	ah, 0			; levy okraj
	jna	@@6
	cmp	ah, 158			; pravy okraj
	jnb	@@6
	pop	ax			; vse OK
	mov	[&x.head], ax		; novy zacatek nibble
	jmp	@@7
@@6:	pop	ax
	or	[&x.crash], 1
@@7:
ENDM

MACRO	MV_TAIL x
LOCAL	@@1, @@2
	lea	di, [&x.body]
	xor	ax, ax
	mov	al, [&x.length]
	shl	ax, 1
	add	di, ax

	cmp	[&x.append], 0
	ja	@@1
	mov	si, di
	dec	si			; umazu konec
	dec	si
	mov	di, ds:[si]
	mov	ax, VIDEO
	mov	es, ax
	mov	ax, BLANK
	stosw
	jmp	@@2
@@1:	mov	si, di			; prodluzuju
	dec	si
	dec	si
	push	ds
	pop	es
	std
	movsw
	dec	[&x.append]
	inc	[&x.length]		; posun posledni pole o 1 dozadu
@@2:
ENDM

MACRO	MV_BODY x
	lea	di, [&x.head]		; pocitam s tim, ze za head je body
	xor	ax, ax			; a presunu to najednou
	mov	al, [&x.length]
	mov	cx, ax
	shl	ax, 1
	add	di, ax
	mov	si, di
	dec	si
	dec	si
	push	ds
	pop	es
	std
	rep movsw
	cld
ENDM

MACRO	TEST_HEAD x, y
LOCAL	@@1, @@2, @@3
	mov	ax, VIDEO
	mov	es, ax

	mov	ax, [&x.head]
	mov	di, ax
	cmp	ax, [&y.head]
	jne	@@1
	or	[&x.crash], 1

@@1:	cmp	ax, [trg.pos]
	jne	@@2
	or	[collect], 1		; prodluzuj
	xor	bx, bx
	mov	bl, [trg.num]
	add	[&x.score], bx
	shl	bl, 2			; 4x sebrane cislo
	add	[&x.append], bl
	jmp @@3

@@2:	cmp	es:[di], BLANK
	je	@@3
	or	[&x.crash], 1
	
@@3:	mov	ax, [&x.color]
	stosw
ENDM

MACRO	MOVE_NIBBLE
LOCAL	@1, @2, @3, @4, @5, @6, @7
	cmp	[p1.lives], 0
	je	@1
	NEW_HEAD p1
@1:	cmp	[p2.lives], 0
	je	@2
	NEW_HEAD p2			; spocitej novou polohu zacatku

@2:	cmp	[p1.lives], 0
	je	@3
	MV_TAIL p1
@3:	cmp	[p2.lives], 0
	je	@4
	MV_TAIL p2			; posun a kontroluj konce

@4:	cmp	[p1.lives], 0
	je	@5
	MV_BODY p1
@5:	cmp	[p2.lives], 0
	je	@6
	MV_BODY p2			; posun a kontroluj kolizi nibble

@6:	cmp	[p1.lives], 0
	je	@7
	TEST_HEAD p1, p2
@7:	cmp	[p2.lives], 0
	je	@8
	TEST_HEAD p2, p1		; kontroluj dosazeni cile
@8:
ENDM

MACRO	CRASH_TEST x, y
LOCAL	@@1
	cmp	[&x.lives], 0
	je	@@1
	cmp	[&x.crash], 0
	je	@@1
	or	[crash], 1		; nastav globalni crash flag
	dec	[&x.lives]
	jnz	@@1			; posledni zivot
	cmp	[&y.lives], 0
	jne	@@1			; druhej jeste zije
	mov	ax, 1
	ret
@@1:
ENDM

MACRO	WAIT_READY
LOCAL	@@1
	lea	si, [wait_msg]
	mov	ax, VIDEO
	mov	es, ax
	mov	di, 2*(25*80+28)
	mov	cx, 24
	cld
	rep movsw
@@1:	mov	ah, 0
	int 16h
	cmp	ah, 1
	jne	@@1
ENDM

MACRO	SETUP_PLAYERS x, pos, dir
	push	ds
	pop	es
	mov	[&x.length], 1
	mov	[&x.append], 5
	mov	ax, &pos
	cld
	lea	di, [&x.head]
	stosw
	stosw
	lea	di, [&x.d_buf]
	mov	es:[di], &dir
	mov	[&x.d_size], 0
	mov	[&x.crash], 0
ENDM

PROC	play_loop
	push	ds
	pop	es
@cr:	call	clear
	WAIT_READY
	call	frame
	push	[level_seg]
	call	show_level
	add	sp, 2
	SETUP_PLAYERS p1, P1_POS, 1
	SETUP_PLAYERS p2, P2_POS, 3
	mov	[trg.num], 0
@tg:	call	new_target
	jnc	@@1
	xor	ax, ax
	ret				; vsechny cile jsou posbirane
@@1:	call	show_status
	mov	[crash], 0
	mov	[collect], 0
	mov	ax, [timer_val]
	mov	[timer], ax

@timer:	cmp	[timer], 0		; cekej na casovac
	ja	@timer
	mov	ax, [timer_val]
	mov	[timer], ax		; nastav novou hodnotu casovace


	call	get_keys		; precti klavesy a dej smery do fronty
	cmp	ax, 0
	je	@mv
	ret				; stisknut Escape

@mv:	MOVE_NIBBLE
	CRASH_TEST p1, p2
	CRASH_TEST p2, p1
	
	cmp	[collect], 0		; if (collect)
	je	@@2
	cmp	[crash], 0		;     if (crash)
	jne	@cr			;         goto cr
	jmp	@tg			;     else goto tg

@@2:	cmp	[crash], 0		; else if (crash)
	jne	@cr			;          goto cr
	jmp	@timer			;      else goto timer
ENDP

PROC	memmove
ARG	d: PTR BYTE, s: PTR BYTE, l: WORD
	mov	cx, [l]
	mov	ax, ds
	mov	es, ax
	mov	si, [s]
	mov	di, [d]
	cld
	cmp	si, di
	ja	@@1
	std
	add	si, cx
	add	di, cx
	dec	si
	dec	di
@@1:	rep movsb
	mov	ax, [d]
	ret
ENDP

PROC    new_timer
	mov	ax, 3508h
	int	21h
	mov	[ofs_08], bx
	mov     [seg_08], es
	lea	dx, int08
	push	ds
	push	cs
	pop	ds
        mov     ax, 2508h
        int     21h
	pop	ds
	mov	[busy], 0
        ret
ENDP

PROC    old_timer
	push	ds
	mov	dx, [ofs_08]
	mov	ds, [seg_08]
	mov	ax, 2508h
	int	21h
	pop	ds
        ret
ENDP

PROC    int08                        ; obsluha casovace
        pushf
        call    dword ptr cs:[ofs_08]
	push	ax
	push	ds
	mov	ax, _data
	mov	ds, ax
        cmp     [busy], 0
        jne     @@2
        inc     [busy]
        cmp	[timer], 0
        jbe	@@1
	dec	[timer]
@@1:	dec	[busy]
@@2:	pop     ds
	pop     ax
	iret
ENDP

	ENDS

	END