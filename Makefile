nib.exe: nib.obj play.obj
	tlink /s nib.obj play.obj

nib.obj: nib.asm
	tasm /zi nib.asm

play.obj: play.asm
	tasm /zi play.asm
